import { useContext, useState, Fragment } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'

import UserContext from '~/contexts/UserContext'

export default () => {
    const { user } = useContext(UserContext)
    const [isExpanded, setIsExpanded] = useState(false)

    let RightNavOptions
    let LeftNavOptions

    if (user.email !== null) {
        RightNavOptions = (
            <Fragment>
                <Link href="/logout"><a className="nav-link">Logout</a></Link>
            </Fragment>
        )
        LeftNavOptions = (
            <Fragment>
                <Link href="/user/categories"><a className="nav-link">Categories</a></Link>
                <Link href="/user/records"><a className="nav-link">Records</a></Link>
                <Link href="/user/charts/balance-trend"><a className="nav-link">Trend</a></Link>
                <Link href="/user/charts/category-breakdown"><a className="nav-link">Breakdown</a></Link>
            </Fragment>
        )
    } else {
        RightNavOptions = null
        LeftNavOptions = null
    }

    return (
        <Navbar expanded={ isExpanded } bg="dark" expand="lg" fixed="top" variant="dark">
            <Link href="/"><a className="navbar-brand">Budget Tracker</a></Link>
            <Navbar.Toggle onClick={ () => setIsExpanded(!isExpanded) } aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto" onClick={ () => setIsExpanded(!isExpanded) }>
                    { LeftNavOptions }
                </Nav>
                <Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) }>
                    { RightNavOptions }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}