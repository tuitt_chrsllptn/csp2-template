import { useState, useEffect } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    return (
        <View title="New Record">
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>New Record</h3>
                    <Card>
                        <Card.Header>Record Information</Card.Header>
                        <Card.Body>
                            <NewRecordForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewRecordForm = () => {
    const [categoryName, setCategoryName] = useState(undefined)
    const [typeName, setTypeName] = useState(undefined)
    const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState('')
    const [categories, setCategories] = useState([])

    const createRecord = (e) => {
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                typeName: typeName,
                amount: amount,
                description: description
            })
        }
        
        fetch(`${ AppHelper.API_URL }/users/add-record`, payload).then(AppHelper.toJSON).then(isSuccessful => {
            if (isSuccessful === true) {
                Swal.fire('Record Added', 'The new record has been successfully created.', 'success')
                Router.push('/user/records')
            } else {
                Swal.fire('Operation Failed', 'The operation failed, try again.', 'error')
            }
        })
    }

    const getTypeCategories = (value) => {
        setTypeName(value)

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({ typeName: value })
        }

        fetch(`${ AppHelper.API_URL }/users/get-categories`, payload).then(AppHelper.toJSON).then(data => {
            setCategories(data)
        })
    } 

    return (
        <Form onSubmit={ (e) => createRecord(e) }>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={ typeName } onChange={ (e) => getTypeCategories(e.target.value) } required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control as="select" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required>
                    <option value selected disabled>Select Category</option>
                    {
                        categories.map((category) => {
                            return (
                                <option key={ category._id } value={ category.name }>{ category.name }</option>
                            )
                        })
                    }
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control type="number" placeholder="Enter amount" value={ amount } onChange={ (e) => setAmount(parseFloat(e.target.value)) } required/>
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description:</Form.Label>
                <Form.Control type="text" placeholder="Enter description" value={ description } onChange={ (e) => setDescription(e.target.value) } required/>
            </Form.Group>
            <Button variant="primary" type="submit">Submit</Button>
        </Form>
    )
}