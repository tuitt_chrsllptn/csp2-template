import { useState, useEffect } from 'react'
import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'

import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }
        }

        fetch(`${ AppHelper.API_URL }/users/get-categories`, payload).then(AppHelper.toJSON).then((categories) => {
            if (categories !== null) {
                setCategories(categories)
            } else {
                Swal.fire('Operation Failed', 'The operation failed, try to refresh the page.', 'error')
            }
        })
    }, [])

    return (
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        categories.map((category) => (
                            <tr key={ category._id }>
                                <td>{ category.name }</td>
                                <td>{ category.type }</td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </View>
    )
}