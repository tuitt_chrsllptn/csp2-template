import { useState, useEffect } from 'react'
import { InputGroup, Form, Col } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import moment from 'moment'

import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    
    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56'
                ],
                hoverBackgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56'
                ]
            }
        ]
    };

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`${ AppHelper.API_URL }/users/get-records-breakdown-by-range`, payload).then(AppHelper.toJSON).then(records => {
            setLabelsArr(records.map(record => record.categoryName))
            setDataArr(records.map(record => record.totalAmount))
        })
    }, [fromDate, toDate])


    return (
        <View title="Category Breakdown">
            <h3>Category Breakdown</h3>
            <Form.Row>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>From</Form.Label>
                    <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                </Form.Group>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </Form.Group>
            </Form.Row>
            <hr/>
            <Pie data={ data } height="100"/>
        </View>
    )
}