import { useState, useEffect } from 'react'
import { InputGroup, Form, Col } from 'react-bootstrap'
import { Line } from 'react-chartjs-2'
import moment from 'moment'

import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])

    const data = {
        labels: labelsArr,
        datasets: [
            {
                label: 'Balance',
                fill: true,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: dataArr,
            }
        ]
    }
    const options = {
        legend: {
            display: false
        }
    }

    const updateChartData = (records) => {
        let balanceValues = []
        let newData

        for (let i = 0; i < records.length; i++) {
            balanceValues.push(records[i].balanceAfterTransaction)
        }

        setLabelsArr(balanceValues)
        setDataArr(balanceValues)
    }

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`${ AppHelper.API_URL }/users/get-records-by-range`, payload).then(AppHelper.toJSON).then(records => {
            updateChartData(records)
        })
    }, [fromDate, toDate])

    return (
        <View title="Balance Trend">
            <h3>Balance Trend</h3>
            <Form.Row>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>From</Form.Label>
                    <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                </Form.Group>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </Form.Group>
            </Form.Row>
            <hr/>
            <Line data={ data } options={ options }  height="100"/>
        </View>
    )
}