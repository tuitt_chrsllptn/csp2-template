import { useState, useContext } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '~/contexts/UserContext'
import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    const [tokenId, setTokenId] = useState(null)
    const { user, setUser } = useContext(UserContext)

    if (user.email !== null) {
        Router.push('/user/records')
    }

    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)

        console.log(response)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-token-id`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
            setUser({ id: data._id, isAdmin: data.isAdmin })
            Router.push('/user/records')
        })
    }

    return (
        <View title={ 'Budget Tracking Login' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Zuitt Budget Tracking</h3>
                    <p>To start, log in by using your Google Account.</p>
                    <GoogleLogin
                        clientId='647742660385-m4lo8lnpksqf61ltb06q2r1uvd6bk3lq.apps.googleusercontent.com'
                        buttonText='Login'
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                </Col>
            </Row>
        </View>
    )
}