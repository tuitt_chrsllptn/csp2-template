import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'

import { UserProvider } from '~/contexts/UserContext'
import AppHelper from '~/app-helper'
import AppNavbar from '~/components/AppNavbar'

import 'bootstrap/dist/css/bootstrap.min.css'

export default ({ Component, pageProps }) => {
    const [user, setUser] = useState({ email: null })

    useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then((userData) => {
            if (typeof userData.email != 'undefined') {
                setUser({ email: userData.email })
            } else {
                setUser({ email: null })
            }
        })
    }, [user.id])

    const unsetUser = () => {
        localStorage.clear()
        setUser({ email: null })
    }

    // Wrap everything inside UserProvider to give access to the context information of user.

    return (
        <UserProvider value={ { user, setUser, unsetUser } }>
            <AppNavbar/>
            <Container>
                <Component { ...pageProps }/>
            </Container>
        </UserProvider>
    )
}