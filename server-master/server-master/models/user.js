const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    categories: [
        {
            name: {
                type: String,
                required: [true, 'Category name is required.']
            },
            type: { // Income or Expense
                type: String,
                required: [true, 'Category type is required.']
            }
        }
    ],
    transactions: [
        {
            categoryName: {
                type: String,
                required: [true, 'Category name is required.']
            },
            type: { // Income or Expense
                type: String,
                required: [true, 'Category type is required.']
            },
            amount: {
                type: Number,
                required: [true, 'Amount is required.']
            },
            description: {
                type: String,
                default: null
            },
            balanceAfterTransaction: {
                type: Number,
                required: [true, 'Balance is required.']
            },
            dateAdded: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model('user', userSchema)